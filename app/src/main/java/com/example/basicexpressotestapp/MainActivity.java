package com.example.basicexpressotestapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    private TextView textView;
    private EditText txtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        txtInput = (EditText) findViewById(R.id.txtInput);

        findViewById(R.id.btnOK).setOnClickListener(this);
        findViewById(R.id.btnOK2).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        final String input = txtInput.getText().toString();

        if (view.getId() == R.id.btnOK) {
            textView.setText(input);
        } else if (view.getId() == R.id.btnOK2) {
            System.out.println("second button pressed");
            Intent intent = com.example.basicexpressotestapp.SecondActivity.newStartIntent(this, input);
            startActivity(intent);
        }
    }
}